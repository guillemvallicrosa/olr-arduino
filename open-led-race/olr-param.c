#include "olr-param.h"

void param_setdefault(struct cfgparam *cfg)
{
  cfg->setted = true;

  cfg->race.startline = true;
  cfg->race.nlap = NUMLAP;
  cfg->race.nrepeat = 1;
  cfg->race.finishline = true;

  // ramps
  cfg->ramps.alwaysOn = true;
  cfg->ramps.num_ramp = 2;

  // ramp 0
  cfg->ramps.ramps[0].init = 27;
  cfg->ramps.ramps[0].center = 40;
  cfg->ramps.ramps[0].end = 53;
  cfg->ramps.ramps[0].high = 1;

  // ramp 1
  cfg->ramps.ramps[1].init = 90;
  cfg->ramps.ramps[1].center = 103;
  cfg->ramps.ramps[1].end = 116;
  cfg->ramps.ramps[1].high = 1;

  cfg->track.nled_total = 178;
  cfg->track.nled_main = 178;  // 240 when boxes length = 60
  cfg->track.nled_aux = 0;        // 60
  cfg->track.init_aux = -1;       // 239
  cfg->track.box_len = BOXLEN;
  cfg->track.box_alwaysOn = false;

  cfg->track.kf = 0.03;  // friction constant
  cfg->track.kg = 0.002;  // gravity constant  - Used in Slope
}
